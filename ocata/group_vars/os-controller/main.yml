---
# General site vars
################################################################################

# list of packages to be installed for OpenStack controller
os_controller_pkgs:
  - python-pip
  - python-keystoneclient
  - python-keystonemiddleware
  - memcached
  - python-memcached
  - mariadb
  - mariadb-server
  - python2-PyMySQL
  - MySQL-python
  - rabbitmq-server
  - openstack-keystone
  - httpd
  - mod_wsgi
  - openstack-glance
  - openstack-nova-api
  - openstack-nova-conductor
  - openstack-nova-console
  - openstack-nova-novncproxy
  - openstack-nova-scheduler
  - openstack-nova-placement-api
  - openstack-neutron
  - openstack-neutron-ml2
  - openstack-neutron-linuxbridge
  - ebtables
  - openstack-dashboard

# list of pip packages to be installed for OpenStack controller
os_controller_pip_pkgs:
  - shade

# mariadb root user:
os_mariadb_root_user: "root"

# mariadb root password:
os_mariadb_root_password: "{{ vault_os_mariadb_root_password }}"

# auth env list: admin user
os_auth_env:
  OS_USERNAME: "admin"
  OS_PASSWORD: "{{ KEYSTONE_PASS }}"
  OS_PROJECT_NAME: "admin"
  OS_USER_DOMAIN_NAME: "Default"
  OS_PROJECT_DOMAIN_NAME: "Default"
  OS_AUTH_URL: "http://{{ os_controller }}:35357/v3"
  OS_IDENTITY_API_VERSION: 3

# auth env list: demo user
os_auth_env_demo:
  OS_USERNAME: "demo"
  OS_PASSWORD: "{{ vault_os_user_demo_password }}"
  OS_PROJECT_NAME: "demo"
  OS_USER_DOMAIN_NAME: "Default"
  OS_PROJECT_DOMAIN_NAME: "Default"
  OS_AUTH_URL: "http://{{ os_controller }}:35357/v3"
  OS_IDENTITY_API_VERSION: 3


# auth dictionary vars duo to the bug in os_user module
# https://github.com/ansible/ansible-modules-core/issues/4520
os_auth_dict:
  username: "admin"
  password: "{{ KEYSTONE_PASS }}"
  auth_url: "http://{{ os_controller }}:35357/v3"
  project_name: "admin"
  user_domain_name: "Default"
  project_domain_name: "Default"
  identity_api_version: 3

# Keystone site vars
################################################################################
# Keystone database credentials
os_keystone_db_name: "keystone"
os_keystone_db_user: "keystone"
os_keystone_db_password: "{{ vault_os_keystone_db_password }}"

# Glance site vars
################################################################################
# Glance database credentials
os_glance_db_name: "glance"
os_glance_db_user: "glance"
os_glance_db_password: "{{ vault_os_glance_db_password }}"

# glance directory for storing images
os_glance_store_dir: "/var/lib/glance/images/"

# Nova site vars
################################################################################
# Nova database credentials
os_nova_db_name: "nova"
os_nova_api_db_name: "nova_api"
os_nova_cell_db_name: "nova_cell0"
os_nova_db_user: "nova"
os_nova_db_password: "{{ vault_os_nova_db_password }}"

# Neutron site vars
################################################################################
# Neutron database credentials
os_neutron_db_name: "neutron"
os_neutron_db_user: "neutron"
os_neutron_db_password: "{{ vault_os_neutron_db_password }}"
os_neutron_vni_start: 1
os_neutron_vni_end: 1000

# Projects
################################################################################
os_project_list:
  - name: "service"
    descr: "Service Project"
    domain: "default"
  - name: "demo"
    descr: "Demo Project"
    domain: "default"

# Users
################################################################################
# When to update password for openstack users
# Options:
# 1. Only when creating users
#    os_user_update_password: "on_create"
# 2. Always
#    os_user_update_password: "always"
#    
os_user_update_password: "on_create"


os_user_list:
  - username: "demo"
    password: "{{ vault_os_user_demo_password }}"
    domain: "default"
    project: "demo"
    role: "user"
  - username: "glance"
    password: "{{ GLANCE_PASS }}"
    domain: "default"
    project: "service"
    role: "admin"
  - username: "nova"
    password: "{{ NOVA_PASS }}"
    domain: "default"
    project: "service"
    role: "admin"
  - username: "placement"
    password: "{{ PLACEMENT_PASS }}"
    domain: "default"
    project: "service"
    role: "admin"
  - username: "neutron"
    password: "{{ NEUTRON_PASS }}"
    domain: "default"
    project: "service"
    role: "admin"

# Network configuration
################################################################################
### provider network setup ###
os_provider_network:
  - name: "provider"
    physical: "provider"
    type: "flat"
    subnet_name: "provider"
    alloc_pool_begin: "172.19.18.64"
    alloc_pool_end: "172.19.18.128"
    dns_list:
      - 8.8.8.8
      - 8.8.4.4
    gateway: "172.18.18.1"
    cidr: "172.19.18.0/24"
    creds: "{{ os_auth_env }}"
    dhcp: "True"

### service networks setup ###
os_networks:
  - name: "selfservice"
    subnet_name: "selfservice"
    dns_list:
      - 8.8.8.8
      - 8.8.4.4
    gateway: "172.16.1.1"
    cidr: "172.16.1.0/24"
    creds: "{{ os_auth_env_demo }}"

### routers setup ###
os_net_router_list:
  - name: "router01"
    interfaces: "selfservice"
    network: "provider"
    creds: "{{ os_auth_env_demo }}"

# Flavors configuration
################################################################################
os_flavor_list:
  - name: "m-tiny"
    vcpu: 1
    ram: 512
    disk: 2
    ephemeral: 0
    swap: 0
    creds: "{{ os_auth_env }}"
  - name: "m-normal"
    vcpu: 2
    ram: 2048
    disk: 10
    ephemeral: 0
    swap: 256
    creds: "{{ os_auth_env }}"
  - name: "m-big"
    vcpu: 4
    ram: 4096
    disk: 30
    ephemeral: 10
    swap: 2048
    creds: "{{ os_auth_env }}"
  - name: "m-huge"
    vcpu: 8
    ram: 8192
    disk: 50
    ephemeral: 20
    swap: 4096
    creds: "{{ os_auth_env }}"

# Images configuration
################################################################################
os_image_download_path: "/tmp/"

os_image_list:
    - name: "cirros"
      format: "qcow2"
      location: "http://download.cirros-cloud.net/0.3.5/"
      filename: "cirros-0.3.5-x86_64-disk.img"
      creds: "{{ os_auth_env }}"
    - name: "cirros-demo"
      format: "qcow2"
      location: "http://download.cirros-cloud.net/0.3.5/"
      filename: "cirros-0.3.5-x86_64-disk.img"
      creds: "{{ os_auth_env_demo }}"
    - name: "centos7-demo"
      format: "qcow2"
      location: "http://cloud.centos.org/centos/7/images/"
      filename: "CentOS-7-x86_64-GenericCloud.qcow2"
      creds: "{{ os_auth_env_demo }}"
    - name: "debian8-demo"
      format: "qcow2"
      location: "http://cdimage.debian.org/cdimage/openstack/current/"
      filename: "debian-8.7.3-20170323-openstack-amd64.qcow2"
      creds: "{{ os_auth_env_demo }}"
