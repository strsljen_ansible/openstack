# OpenStack Ansible simple automation
This project is intended as PoC of OpenStack Ansible automation from stratch.
It utilizes latest CentOS 7 and OpenStack ocata release.

Minimal Ansible version required: 2.3

The initial setup follows:
* Controller node (single)
* Compute nodes (multiple)
* Block node (single)
* Object node (single)

It follows documentation at:
https://docs.openstack.org/ocata/install-guide-rdo/

Automation is set in order to fully utilize group_vars and thus makes it easy
to add new compute nodes in the stack.
Also, all important config changes are to be done by changing values in
group_vars.

# Prerequisites and usage

## Prerequisites

* Prepare 1 controller node, 1 or more compute nodes, 1 object and 1 block node
* OS: Centos 7
* Networking: 4 network interfaces, 3 of them free for OpenStack

# Usage

```
ansible-playbook prepare_nodes.yml -D
```

```
ansible -a "reboot" os-all
```

```
ansible-playbook openstack.yml -D
```

## Project phases

### Phase 1

Initial setup of controller node and compute nodes.

Components:
* Identity service (keystone)
* Image service (glance)
* Compute service (nova)
* Networking service (neutron)
* Dashboard (horizon)

### Phase 2

Configuration of networks with Open vSwitch

### Phase 3

Creating instances 

### Phase 4

Utilizing block and object storage components.

### Phase 5


Currently: phase 1 is done. Phase 2 is about to start.

## Networking

https://docs.openstack.org/ocata/install-guide-rdo/environment-networking.html

Four interfaces required: eth0-eth3:

* eth0 is host IP
* eth1 is management network as described:
* eth2 TBD in Open vSwitch phase
* eth3 TBD in Open vSwitch phase

## Notes

Making the automation more configurabile:
* make every possible option as a variable in group_vars (for example, service
  port numbers
* making installation of some components controllable by using flags
* IMPORTANT: proper documentation is yet to be written. At the moment, the
  project is in early development phase
